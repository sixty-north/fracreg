.. fracreg-design documentation master file, created by
   sphinx-quickstart on Mon Feb 16 08:41:52 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

==================
``fracreg`` Design
==================

This document covers the requirements, constraints, and design of the
``fracreg`` project. ``fracreg`` is a tool (or, more likely, a set of
tools) for entering and storing detailed information about a bone
fracture and the medical procedures used to treat it. Generally
speaking, it is intended for use after a surgical procedure when the
relevant information is freshest in the surgeon's mind. While most
surgical facilities already capture certain information about
fractures and their related procedures, this information is often
coarse, high-level, and geared more towards administrative
(i.e. insurance) needs rather than the needs of medical
research. ``fracreg`` will deal with detailed information from the
realm of fracture research and treatment, with the goals of a)
capturing all relevant information and b) making this data available
for and amenable to analysis.

Contents:

.. toctree::
   :maxdepth: 2

   domain

Notes
=====

* Development of OpenEHR standard fracture archetype

* interfacing with DIPS

* reading patient data from various databases.

* Mimicking existing ORPlan fracture module

* BuyPass / authentication to send data

* Avoid redundant data entry (apparently very high priority)

* Deployment environments
  * prototype stage: the hardware available to our group
  * SuS deployment: the IKT Vest infrastructure
  * Wider HelseVest: still the IKT Vest infrastructure
  * Beyond: arbitrary deployment environments

* Technology choices
  * Web-based to simplify deployment
    * Only browsers are targeted at client end -> simple upgrade, etc.
    * Support multiple databases?
    * Be able to copy data to multiple "official" repositories/OpenEHR
      repositories.

* Prototype
  * timeline
  * goals
  * technologies: Pyramid, backbone, bootstrap
    * Using things that are well-known and established. Don't need to
      be on any bleeding edges. Yet.

* Various services
  * AO code / images
  * facilities server: from what facilities can we access previous
    procedures? How do these correlate to "second procedure" reasons?
  * Previous procedures: we need to be able to fetch these, getting as
    much available info from them as possible.

* Fast keyboard navigation of the tool for expert users
* Intuitive, easy-to-learn mouse navigation for new users
* Need "checkout" screen where we show the user what they've entered
  prior to submission.
  * This should try to highlight when they haven't entered important
    optional information. It's apparently common for surgeons to leave
    this information out, but it's really important for research
    purposes.

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
