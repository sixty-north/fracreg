========
 Domain
========

The domain of `fracreg` deals with fractures, procedures related to
those fractures, patient information, and so forth.

Domain dictionary
=================

complication
    An unexpected condition or occurrence which may neccesitate a
    reoperation. Examples include infections, dislocation, and
    hematoma.

dislocation
    A condition where a bone is moved out of its normal position but
    is not necessarily fractured.

facility
    A location (typically a hospital) where a procedure is performed.

fracture
    A specific break to a bone occurring in a specific incident. Data:
    AO code, bone, dislocation description, open classification,
    pathology

incident
    An event at a specific time which resulted in one or more
    fractures. Data: date, hour, treatment delay

method
    A specific surgical technique applied in a procedure for treating
    or stabilizing a fracture, e.g. a plate or a pin.

open classification
    A classification of an open fracture, typically classified using
    the `Gustilo system
    <http://en.wikipedia.org/wiki/Gustilo_open_fracture_classification>`_.

operation
    A medical operation intended to treat a specific fracture.

patient
    A person with one or more fractures.

pathology
    **TODO**

procedure
    Activity associated with an operation which support the
    implementation of the methods being applied. **TODO**: Does this
    really capture the difference between procedures and methods?

reoperation
    A procedure for a fracture which is subsequent to the first
    procedure for the fracture. Reoperations may be planned (i.e. part
    of the normal course of treating the fracture) or unplanned
    (i.e. necessary for treating an infection or other complication.)

secondary procedure
    see: reoperation
