# doctors
## /doctor

POST,json - add new doctor

## /doctor/<id>

GET,json - get doctor info with specific ID
GET,html - display doctor with ID

# patients

## /patient

POST,json - add new patient

## /patient/<id>

GET,json - get patient info with ID
GET,html - display patient with ID

# records

## /record

POST,json - create a new record

## /record/<id>

GET,json - get record info with ID
GET,html - display record with ID

## /records

GET - list of record IDs

# TypeScript stuff

Compiling typescript files:
```
find . -name "*.ts" | xargs /usr/local/bin/node /usr/local/bin/tsc --module amd -t ES5
```
