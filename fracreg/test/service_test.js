'use strict';

describe('fracregServices function', function() {

    describe('PreviousProcedures', function() {

        var previousProcedures, httpBackend;

        beforeEach(function() {
            angular.mock.module('fracregServices');
        });

        beforeEach(function () {
            inject(
                function($httpBackend, PreviousProcedures) {
                    httpBackend = $httpBackend;
                    previousProcedures = PreviousProcedures;
                }
            );

            // angular.mock.inject(function ($injector) {
            //     $httpBackend = $injector.get('$httpBackend');
            //     previousProcedures = $injector.get('PreviousProcedure');
            // })
        //   // Set up the mock http service responses
        //     $httpBackend = $injector.get('$httpBackend');

        //     // Get hold of a scope (i.e. the root scope)
        //     $rootScope = $injector.get('$rootScope');

        //     $q = $injector.get('$q');

        //     $resource = $injector.get('$ngResource');

        //     // The $controller service is used to create instances of controllers
        //     var $controller = $injector.get('$controller');

        //     createController = function() {
        //         return $controller('FracRegCtrl', {'$scope' : $rootScope });
        //     };
        });

        it('should set the default `previousFacility` to null', function() {
            expect(1).toBe(1);
        });
    });
});
