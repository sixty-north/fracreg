'use strict';

describe('fracregController function', function() {

    describe('FracRegCtrl', function() {

        var $rootScope;
        var $scope;
        var $q;
        var $modal;
        var $httpBackend;
        var mockFacilitiesService;
        var queryDeferred;

        beforeEach(function() {
            angular.mock.module('fracregControllers');
        });

        beforeEach(inject(function(_$rootScope_, _$httpBackend_, _$q_, _$modal_) {
            $q = _$q_;
            $modal = _$modal_;
            $rootScope = _$rootScope_;
            $httpBackend = _$httpBackend_;

            $httpBackend.whenGET('/treatment_delays').respond([
                {id: 'test-delay-1', text: 'Test delay 1'}
            ]);
            $httpBackend.whenGET('/open_fracture_classifications').respond([
                {id: 'frac-class-1', text: 'Fracture Class 1'}
            ]);
            $httpBackend.whenGET('/pathologies').respond([
                {id: 'pathology-1', text: 'Pathology 1'}
            ]);
            $httpBackend.whenGET('/main_methods').respond([
                {id: 'main-method-1', text: 'Main Method 1'}
            ]);
            $httpBackend.whenGET('/additional_procedures').respond([
                {id: 'additional-procedure-1', text: 'Additional Procedure 1'}
            ]);
            $httpBackend.whenGET('/repositioning').respond([
                {id: 'repositioning-1', text: 'Repositioning 1'}
            ]);
            $httpBackend.whenGET('/cast').respond([
                {id: 'cast-1', text: 'Cast 1'}
            ]);
            $httpBackend.whenGET('/wound_treatment').respond([
                {id: 'wound-treatment-1', text: 'Wound Treatment 1'}
            ]);
            $httpBackend.whenGET('/transplants').respond([
                {id: 'transplant-1', text: 'Transplant 1'}
            ]);
            $httpBackend.expectGET('/facilities').respond([
                {code: 'test-facility', name: 'Test facility'}
            ]);
            $httpBackend.whenGET('/previous_procedures/test-facility').respond([
                {id: 1,
                 data: {
                     side: 'venstre',
                     procedure: 'test procedure',
                     date: '2014-12-12',
                     time: '10:01',
                     delay: 'less-than-three',
                     ao_code: '33A3',
                     dislocation: null,
                     open_fracture_classification: 'type-2',
                     pathology: null,
                     pathology_description: ''
                 }
                }
            ]);
            $httpBackend.whenGET('/secondary_procedure_reasons/test-facility/none').respond([
                (0, 'reason-1'),
                (1, 'reason-2')
            ]);
            $httpBackend.whenGET('/secondary_procedure_reasons/test-facility/simple').respond([
                (0, 'reason-1'),
                (1, 'reason-2')
            ]);
            $httpBackend.whenGET('/secondary_procedure_reasons/test-facility/serious').respond([
                (0, 'reason-1'),
                (1, 'reason-2')
            ]);
            $httpBackend.whenGET('/ao_codes').respond([{id: '31A3', text: 'something'}]);
        }));

        beforeEach(inject(function($controller) {
            $scope = $rootScope.$new();

            $controller('FracRegCtrl', {$scope: $scope});
        }));

        it('should start with empty facilities', function () {
            expect($scope.facilities).toEqual([]);
        });

        it('should eventually get some facilities', function () {
            $httpBackend.flush();
            expect($scope.facilities.length).toBeGreaterThan(0);
        });

        it('should set the default `previousFacility` to null', function() {
            expect($scope.previousFacility).toBe(null);
        });

        it('should set default `previousProcedure` to null', function() {
            expect($scope.previousProcedure).toBe(null);
        });

        it('should set the default previousExternalFacility to false', function () {
            expect($scope.previousExternalFacility).toBe(false);
        });
        it('should set the default noneComplications to []', function () {
            expect($scope.noneComplications).toEqual([]);
        });
        it('should set the default simpleComplications to []', function () {
            expect($scope.simpleComplications).toEqual([]);
        });
        it('should set the default seriousComplications to []', function () {
            expect($scope.seriousComplications).toEqual([]);
        });

        it('should set the default mainMethod to null', function () {
            expect($scope.mainMethod.selected).toBe(null);
            expect($scope.mainMethod.options).toEqual([]);
        });
        it('should eventually get some main-method options', function () {
            $httpBackend.flush();
            expect($scope.mainMethod.options.length).toBeGreaterThan(0);
        });
        it('should set the default secondaryMethods to []', function () {
            expect($scope.secondaryMethods.selected).toEqual([]);
            expect($scope.secondaryMethods.options).toEqual([]);
        });
        it('should eventually get some secondaryMethod options', function () {
            $httpBackend.flush();
            expect($scope.secondaryMethods.options.length).toBeGreaterThan(0);
        });
        it('should set the default additionalProcedures to []', function () {
            expect($scope.additionalProcedures).toEqual([]);
        });
        it('should eventually get some additional procedures', function () {
            $httpBackend.flush();
            expect($scope.additionalProcedures.length).toBeGreaterThan(0);
        });
        it('should set the default repositioning to none', function () {
            expect($scope.repositioning.selected).toBe(null);
        });
        it('should set the default arthroscopicallyAssisted to false', function () {
            expect($scope.arthroscopicallyAssisted).toBe(false);
        });
        it('should set the default cast to none', function () {
            expect($scope.cast.selected).toBe(null);
        });
        it('should set the default woundTreatment to none', function () {
            expect($scope.woundTreatment.selected).toBe(null);
        });
        it('should set the default transplant to none', function () {
            expect($scope.transplant.selected).toBe(null);
        });
        it('should set the default message to empty', function () {
            expect($scope.message).toBe('');
        });

        it('should set the default aoInfo to null', function () {
            expect($scope.aoInfo).toBe(null);
        });
        it('should set the default hour to null', function () {
            expect($scope.hour).toBe(null);
        });
        it('should set the default date to null', function () {
            expect($scope.date).toBe(null);
        });

        it('should set the default selected treatmentDelay to empty', function () {
            expect($scope.treatmentDelay.selected).toBe(null);
            expect($scope.treatmentDelay.options).toEqual([]);
        });

        it('should eventually get some treatment-delay options', function () {
            $httpBackend.flush();
            expect($scope.treatmentDelay.options.length).toBeGreaterThan(0);
        });

        it('should set the default selected dislocation to null', function () {
            expect($scope.dislocation.selected).toBe(null);
            expect($scope.dislocation.options).toEqual([]);
        });

        it('should set the default selected openFractureClassification to null', function () {
            expect($scope.openFractureClassification.selected).toBe(null);
            expect($scope.openFractureClassification.options).toEqual([]);
        });

        it('should eventually get some fracture classification options', function () {
            $httpBackend.flush();
            expect($scope.openFractureClassification.options.length).toBeGreaterThan(0);
        });

        it('should set the default selected pathology to empty list', function () {
            expect($scope.pathology.selected).toEqual([]);
            expect($scope.pathology.options).toEqual([]);
        });

        it('should eventually get some pathology options', function () {
            $httpBackend.flush();
            expect($scope.pathology.options.length).toBeGreaterThan(0);
        });

        it('should set the default pathologyDescription to null', function () {
            expect($scope.pathologyDescription).toBe(null);
        });

        describe('When a previous facility is selected', function () {
            beforeEach(function () {
                $httpBackend.flush();
                expect($scope.facilities.length).toBeGreaterThan(0);
                $scope.$apply($scope.previousFacility = $scope.facilities[0]);
                $httpBackend.flush();
            });

            it('previous procedures get populated', function () {
                expect($scope.previousProcedures.length).toBeGreaterThan(0);
            });

            describe('and then deselected', function () {
                beforeEach(function() {
                    $scope.$apply($scope.previousFacility = null);
                });

                it('the previous procedures are cleared', function () {
                    expect($scope.previousProcedures.length).toEqual(0);
                });
            });
        });

        describe('When an AO Code is selected', function () {
            it('the dislocation options get populated', function () {
                // TODO
            });
        });
    });


    // TODO: Selecting a previousProcedure updates the appropriate model elements.

});
