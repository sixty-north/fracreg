from pyramid.view import view_config


@view_config(route_name='main_methods',
             request_method='GET',
             renderer='json')
def main_method(request):
    """Get dummy tree of main methods.
    """
    methods = [
        ('glideskrue', 'Glideskrue'),
        ('plate, vanlig', 'Plate, vanlig'),
        ('pinner, metall', 'Pinner, metall'),
        ('sement', 'Sement'),
        ('skruer', 'Skruer'),
        ('cerclage', 'Cerclage'),
    ]
    return [{'id': k, 'text': v} for k, v in methods]


@view_config(route_name='additional_procedures',
             request_method='GET',
             renderer='json')
def additional_procedures(request):
    data = [
        ('evacuated-deep-hematoma', 'Evakuert dypt hematom'),
        ('evacuated-superficial-hematoma', 'Evakuert overfladisk hematom'),
        ('removed-ostemat', 'Fjernet ostemat'),
        ('removed-ex-fix', 'Fjernet ex-fix'),
        ('fasciotomy', 'Fasciotomi'),
        ('osteotomy', 'Ostotomi, korr. akse/rot/lengde'),
        ('repositioning-partial-prosthesis',
         'Reponering av hemiprotese (husk åpen/lukket)'),
        ('repositioning-full-prosthesis',
         'Reponering av totalprotese (husk åpen/lukket)'),
        ('delayed-skin-suture', 'Forsinket hudsutur'),
        ('girdlestone', 'Girdlestone'),
        ('other', 'Annen'),
    ]

    return [{'id': k, 'text': v} for k, v in data]


@view_config(route_name='repositioning',
             request_method='GET',
             renderer='json')
def repositioning(request):
    data = [
        ('none', 'Ingen'),
        ('closed', 'Lukket'),
        ('open', 'Åpen'),
    ]
    return [{'id': k, 'text': v} for k, v in data]


@view_config(route_name='cast',
             request_method='GET',
             renderer='json')
def cast(request):
    data = [
        ('none', 'Ingen'),
        ('laske', 'Laske'),
        ('circular', 'Sirkulær'),
    ]
    return [{'id': k, 'text': v} for k, v in data]


@view_config(route_name='wound_treatment',
             request_method='GET',
             renderer='json')
def wound_treatment(request):
    data = [
        ('none', 'Ingen'),
        ('without-vac', 'u/VAC'),
        ('with-vac', 'm/VAC'),
    ]
    return [{'id': k, 'text': v} for k, v in data]


@view_config(route_name='transplants',
             request_method='GET',
             renderer='json')
def transplants(request):
    data = [
        ('none', 'Ingen'),
        ('cancellous-iliac-crest', 'Spongiøst bein fra hoftekam'),
        ('corticocancellous-iliac-crest', 'Kortikospongiøst bein fra hoftkam'),
        ('cancellous-other', 'Spongiøst bein, annen lokalisasjon'),
        ('corticocancellous-other', 'Kortikospongiøst bein, annon lokalisasjon'),
        ('allograft', 'Allograft bein'),
        ('artificial-bone', 'Kunstig beinvev (Norian o.l.)'),
        ('autograft',  'Autograft sene'),
        ('autograft-not-vascularized', 'Autograft knokkel (ikke vaskulisert)'),
        ('autograft-vascularized', 'Autograft vaskulisert knokkel'),
        ('partial-skin', 'Delhud'),
        ('full-skin', 'Fullhud'),
        ('fasciocutaneous-patch', 'Fasciocutan lapp'),
        ('vascularized-skin-patch', 'Vakularisert hudlapp med muskel og bein'),
        ('cartilage', 'Brusk'),
        ('periosteum', 'Periost'),
    ]

    return [{'id': k, 'text': v} for k, v in data]
