import datetime
import sqlite3

import decorator
from pyramid.events import ApplicationCreated, subscriber
from pyramid.httpexceptions import HTTPOk
from pyramid.view import view_config

# TODO: see
# http://compiletoi.net/slides/europython2013-pyramid/#/step-1 for
# ideas on how to encapsulate this. I think that using configuration
# includes we can make the feedback system entirely encapsulated. What
# we'll do is have a list of "modules" in development.ini that say
# what to configure *by string*.


@subscriber(ApplicationCreated)
def application_created_subscriber(event):
    db_filename = event.app.registry.settings['feedback_database']
    conn = sqlite3.connect(db_filename)
    conn.execute(
        'CREATE TABLE IF NOT EXISTS '
        'feedback(name text, message text, ts timestamp)')


@decorator.decorator
def db_client(f, factory, request):
    db_filename = request.registry.settings['feedback_database']
    conn = sqlite3.connect(db_filename)
    try:
        with conn:
            return f(conn, request)
    finally:
        conn.close()


@view_config(route_name='feedback',
             request_method='POST')
@db_client
def submit_feedback(conn, request):
    data = request.json_body
    name = data['name']
    message = data['message']

    conn.execute(
        'INSERT INTO feedback VALUES(?, ?, ?)',
        (name, message, datetime.datetime.now()))

    return HTTPOk()


@view_config(route_name='feedback',
             request_method='GET',
             renderer='feedback.mak')
@db_client
def display_feedback(conn, request):
    fb = list(conn.execute('SELECT * FROM feedback'))
    return {'feedback': fb}
