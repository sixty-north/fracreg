from collections import namedtuple

from pyramid.view import view_config


@view_config(route_name='home', renderer='../templates/fracreg.mak')
def my_view(request):
    return {'project': 'fracreg'}


# Previous procedure stuff

PROCEDURES = {
    'sus-sentral': [
        (0,
         'venstre', 'Inserted pin, proximal femur',
         '2003-10-04', 12,
         'less-than-three', '31A1',
         'lumbal', 'type-1',
         ['metastasis'], 'Some notes'),
        (1,
         'høyre', 'Added plate, femoral shaft',
         '2010-03-14', 3,
         'six-to-twelve', '32A2',
         'hofte', 'type-3b',
         ['local-infection', 'irradiated-bone'], 'more notes'),
        (2,
         'venstre', 'Cement, distal femur',
         '2012-09-23', 19,
         'greater-than-forty-eight', '33A3',
         None, None,
         [], ''),
    ],
    'sus-poliklinikk': [
        (0,
         'venstre', 'Fractured distal femur',
         '2003-09-04', 10,
         'less-than-three', '33A3',
         None, 'type-2',
         [], ''),
    ],
}

FIELDS = (
    "id",
    "side",
    "procedure",
    "date",
    "hour",
    "delay",
    "ao_code",
    "dislocation",
    "open_fracture_classification",
    "pathology",
    "pathology_description",
)


@view_config(route_name='previous_procedures',
             request_method='GET',
             renderer='json')
def previous_procedures(request):
    """Get some dummy data on previous procedures.

    We'll replace this someday with access to real records.
    """

    facility = request.matchdict['facility']
    procedures = PROCEDURES[facility]

    return [dict(zip(FIELDS, proc))
            for proc in procedures]


class Facility:
    def __init__(self, code, name):
        self.code = code
        self.name = name

sentral = Facility('sus-sentral', 'SuS Sentral')
poliklinikk = Facility('sus-poliklinikk', 'SuS Poliklinikk')
external = Facility('external', 'External')


@view_config(route_name='facilities',
             request_method='GET',
             renderer='json')
def facilities(request):
    return [{'code': f.code, 'name': f.name}
            for f in [sentral, poliklinikk]]


ProcedureReason = namedtuple('ProcedureReason', ['name', 'facilities'])


PROCEDURE_REASONS = {
    'none': {
        0: ProcedureReason('Midlertidig beh. pol',
                           {poliklinikk, external}),
        1: ProcedureReason('Endelig primærfiksasjon (planlagt)',
                           {sentral, external}),
        2: ProcedureReason('Rutinefjerning',
                           {sentral, external}),
        3: ProcedureReason('Primær bløtdelsskade (inkl. oppfølgning)',
                           {sentral, external}),
        4: ProcedureReason('Annen (ikke kompl.)',
                           {poliklinikk, sentral, external}),
    },
    'simple': {
        0: ProcedureReason('Mislykket kons. beh. ved poliklinikk',
                           {poliklinikk, external}),
        1: ProcedureReason('Mislykket kons. beh. på opstue',
                           {sentral, external}),
        2: ProcedureReason('Infeksjon. overfladisk (inkl. oppfølg.)',
                           {sentral, poliklinikk, external}),
        3: ProcedureReason('Hematom utenfor fascie',
                           {sentral, poliklinikk, external}),
        4: ProcedureReason('Refraktur uten ostemat. (adekv. traume)',
                           {sentral, poliklinikk, external}),
        5: ProcedureReason('Ubehag av oste. mat.',
                           {sentral, external}),
        6: ProcedureReason('Annen (lett kompl.)',
                           {poliklinikk, external}),
    },
    'serious': {
        0: ProcedureReason('Sårruptur',
                           {sentral, poliklinikk, external}),
        1: ProcedureReason('Infeksjon. dyp (inkl. oppfølgning)',
                           {sentral, poliklinikk, external}),
        2: ProcedureReason('Hematom under fascie',
                           {sentral, poliklinikk, external}),
        3: ProcedureReason('Refraktur uten ostemat. (IKKE adekv. traume)',
                           {sentral, poliklinikk, external}),
        4: ProcedureReason('Implantatnær brudd',
                           {sentral, external}),
        5: ProcedureReason('Osteonekrose',
                           {sentral, external}),
        6: ProcedureReason('Sen/manglende tilheling',
                           {sentral, poliklinikk, external}),
        7: ProcedureReason('Tilhelet i feil stilling (malunion)',
                           {sentral, poliklinikk, external}),
        8: ProcedureReason('Posttraumatisk artrose',
                           {sentral, external}),
        9: ProcedureReason('Ostemat skåret gjennom leddflate',
                           {sentral, external}),
        10: ProcedureReason('Osteosyntesesvikt',
                            {sentral, poliklinikk, external}),
        11: ProcedureReason('Løsning av protese',
                            {sentral, external}),
        12: ProcedureReason('Luksasjon hemiprotese',
                            {sentral, external}),
        13: ProcedureReason('Luksasjon totalprotese',
                            {sentral, external}),
        14: ProcedureReason('Sekondær bløtdelsskade (inkl. oppfølgning)',
                            {sentral, external}),
        15: ProcedureReason('Annen (alv. kompl.)',
                            {sentral, poliklinikk, external}),
    },
}


def _get_reasons(level, location):
    """Find the procedure reasons applicable to `level` in `location`.

    This basically just queries PROCEDURE_REASONS and formats the
    results for sending back to a JSON client.

    Generates a sequence of tuples: [(id, reason), . . .]
    """
    for reason_id, reason in PROCEDURE_REASONS[level].items():
        reason_locations = set(f.code for f in reason.facilities)
        if location in reason_locations:
            yield {'id': reason_id, 'text': reason.name}


@view_config(route_name='secondary_procedure_reasons',
             request_method='GET',
             renderer='json')
def get_procedure_reasons(request):
    return sorted(_get_reasons(request.matchdict['level'],
                               request.matchdict['location']),
                  key=lambda r: r['text'])
