import os.path

import pkg_resources
from pyramid.httpexceptions import HTTPNotFound
from pyramid.response import Response
from pyramid.view import view_config
import yaml

from fracreg.util import memoize


@memoize
def ao_database():
    """Get the AO Code database.

    This is really just a trick for reading the the database once
    without using a global variable.

    For details on the structure, see 'static/ao_codes.json'.

    """
    filename = pkg_resources.resource_filename(
        'fracreg', '/static/ao_codes.yaml')
    with open(filename, 'rt', encoding='utf-8') as f:
        ao_db = yaml.load(f)
    return ao_db


def ao_classifications(db, prefix='', names=None):
    """Generate a sequence of AO classification of the form (CODE,
    [NAME,...], DESCRIPTION) built from an AO code database.
    """
    names = names or []

    for name, body in db.items():
        if name == '_classifications':
            for code, desc in body.items():
                yield (prefix + str(code), names, desc)
        elif not name.startswith('_'):
            yield from ao_classifications(body,
                                          prefix + str(body.get('_prefix', '')),
                                          names + [name])


@view_config(route_name='ao_code_tree',
             request_method='GET',
             renderer='json')
def ao_codes(request):
    """Get the full tree of AO codes.
    """
    return [{'id': code,
             'text': ' / '.join(names + [description])}
            for (code, names, description)
            in ao_classifications(ao_database())]


@view_config(route_name='ao_code',
             request_method='GET',
             renderer='json')
def ao_code(request):
    """Get the AO info with the specified code.
    """
    req_code = request.matchdict['code']
    return [{'id': code,
             'text': ' / '.join(names + [description])}
            for (code, names, description) in ao_classifications(ao_database())
            if code == req_code][0]


@view_config(route_name='ao_image',
             request_method='GET')
def ao_image(request):
    ao_code = request.matchdict['ao_code']

    image_filename = pkg_resources.resource_filename(
        'fracreg', '/static/ao-images/{}.jpg'.format(ao_code))

    if not os.path.exists(image_filename):
        raise HTTPNotFound(
            'No image file for AO code {}, '
            'or invalid code.'.format(ao_code))

    with open(image_filename, 'rb') as f:
        image_data = f.read()

    resp = Response(body=image_data,
                    content_type='image/jpeg')
    return resp


@view_config(route_name='treatment_delays',
             request_method='GET',
             renderer='json')
def treatment_delays(request):
    data = [
        ("unknown", "Ukjent"),
        ("less-than-three", "Under 3 timer"),
        ("three-to-six", "3-6 timer"),
        ("six-to-twelve", "6-12 timer"),
        ("twelve-to-twenty-four", "12-24 timer"),
        ("twenty-four-to-forty-eight", "24-48 timer"),
        ("greater-than-forty-eight", "Mer enn 48 timer"),
    ]

    return [{'id': k, 'text': v} for k, v in data]


@view_config(route_name='dislocations',
             request_method='GET',
             renderer='json')
def dislocations(request):
    data = {
        'lumbal': 'Lumbal',
        'lumbosacral': 'Lumbosacral',
        'symfysesprent-bekken': 'Symfysespreng (bekken)',
        'sacroileacal': 'Sacroileacal',
        'hofte': 'Hofte'
    }
    return [{'id': k, 'text': v} for k, v in data.items()]


@view_config(route_name='open_fracture_classifications',
             request_method='GET',
             renderer='json')
def open_fracture_classifications(request):
    data = [
        ('type-1', 'I: Mindre enn 1-2 cm. rent'),
        ('type-2', 'II: Mer enn 1-2 cm.'
         'Bejskden bløtdelsskade og kommunisjon'),
        ('type-3a', 'IIIa: Høyenergiskade, '
         'uttalt skade hud og bløtdeler, '
         'kontaminasjon, bløtdeksdekning.'),
        ('type-3b', 'IIIb: Som IIIa men uten bløtdelsdekning'),
        ('type-3c', 'IIIc: Som IIIa/b med karskade som krever kirurgi'),
    ]
    return [{'id': k, 'text': v} for k, v in data]


@view_config(route_name='pathologies',
             request_method='GET',
             renderer='json')
def pathologies(request):
    data = [
        ('benign-primary-tumor', 'Godartet primærtumor'),
        ('metastasis', 'Metastase'),
        ('malignant-primary-tumor', 'Ondartet primærtumor'),
        ('local-infection', 'Lokal infeksjon'),
        ('osteogenesis-imperfecta', 'Osteogenesis imperfecta'),
        ('irradiated-bone', 'Strålbehandlet bein'),
        ('fracture-near-implant', 'Implantanær fraktur'),
        ('other', 'Annet')
    ]
    return [{'id': k, 'text': v} for k, v in data]
