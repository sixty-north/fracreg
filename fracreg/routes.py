def configure_routes(config):
    config.add_route('add_fracture', '/add_fracture')
    config.add_route('home', '/')

    # injury info routes
    config.add_route('ao_image', '/ao_image/{ao_code}')
    config.add_route('ao_code_tree', '/ao_codes')
    config.add_route('ao_code', '/ao_codes/:code')
    config.add_route('treatment_delays', '/treatment_delays')
    config.add_route('dislocations', '/dislocations/{ao_code}')
    config.add_route('open_fracture_classifications',
                     '/open_fracture_classifications')
    config.add_route('pathologies', '/pathologies')

    # procedure-info routes
    config.add_route('main_methods', '/main_methods')
    config.add_route('additional_procedures', '/additional_procedures')
    config.add_route('transplants', '/transplants')

    # previous-procedure routes
    config.add_route('facilities', '/facilities')
    config.add_route('previous_procedures', '/previous_procedures/:facility')
    config.add_route('secondary_procedure_reasons',
                     '/secondary_procedure_reasons/:location/:level')
    config.add_route('repositioning', '/repositioning')
    config.add_route('cast', '/cast')
    config.add_route('wound_treatment', '/wound_treatment')

    config.add_route('feedback', '/feedback')

    config.add_static_view(name='test', path='test')

    config.scan()
