/// <reference path="lib/angularjs/angular.d.ts"/>

'use string';

module FracReg.App {

    var fracregApp: angular.IModule = angular.module(
	'fracregApp',
	[
            'aoSelectionControllers',
	    'fracregControllers',
	    'fracregFeedbackControllers',
	    'fracregDirectives',
	    'isteven-multi-select'
	]);
    
}
