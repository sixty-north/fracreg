/// <reference path="lib/angularjs/angular.d.ts" />
/// <reference path="lib/angularjs/angular-resource.d.ts" />
/// <reference path="lib/underscore/underscore.d.ts" />
/// <reference path="controllers/fracregControllers.ts" />

module FracReg.Service {

    var fracregServices: angular.IModule = angular.module(
        'fracregServices',
        ['ngResource']);

    fracregServices.factory(
	'PreviousProcedures',
	['$resource',
	 function($resource: angular.resource.IResourceService) {
	     return $resource('/previous_procedures/:facility', {}, {
		 query: {method:'GET',
			 params: {},
			 isArray: true,

			 // This replaces the string dates with Date objects.
			 transformResponse: (data, headersGetter): FracReg.Controller.Procedure => {
			     var response = angular.fromJson(data);
			     response = _.map<{date: any}, {date: any}>(
				 response,
				 (r) => {
				     r.date = new Date(r.date);
				     return r;
				 }
			     );
			     return response;
			 }
			}
             });
	 }]);

    fracregServices.factory(
	'Facilities',
	['$resource',
	 function($resource: angular.resource.IResourceService){
             return $resource('/facilities', {}, {
		 query: {method:'GET', params:{}, isArray:true}
             });
	 }]);
    
    fracregServices.factory(
	'SecondaryProcedureReasons',
	['$resource',
	 function($resource: angular.resource.IResourceService){
             return $resource('/secondary_procedure_reasons/:location/:level', {}, {
		 query: {method:'GET', isArray:true}
             });
	 }]);
    
    fracregServices.factory(
	'AOCodes',
	['$resource',
	 function($resource: angular.resource.IResourceService){
             return $resource('/ao_codes/:code');
	 }]);
    
    fracregServices.factory(
	'Dislocations',
	['$resource',
	 function($resource: angular.resource.IResourceService){
             return $resource('/dislocations/:ao_code');
	 }]);
}
