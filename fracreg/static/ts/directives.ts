/// <reference path="lib/angularjs/angular.d.ts"/>
/// <reference path="lib/bootstrap-table.d.ts"/>
/// <reference path="lib/snap.svg/snapsvg.d.ts"/>
/// <reference path="controllers/fracregControllers.ts"/>

'use strict';

/* Directives */

module FracReg.Directive {

    var fracregDirectives: angular.IModule = angular.module(
	'fracregDirectives',
	[
	    'ngSanitize',
	    'ui.select'
	]);

    fracregDirectives.directive(
	'fracregFacilitiesTable',
	[() => {
            function link(scope: FracReg.Controller.FracRegScope, element: JQuery, attrs: angular.IAttributes) {
		element.data('striped', 'true');
		element.data('classes', 'table-condensed table');
		element.data('click-to-select', 'true');
		element.data('single-select', 'true');

		element.bootstrapTable({
                    data: [],
                    columns: [{
			field: 'state',
			checkbox: 'true'
                    }, {
			field: 'name',
			title: 'Navn'
                    }],
                    formatNoMatches: () => { return ""; }
		});

		scope.$watch('facilities', () => {
                    if (scope.facilities) {
			element.bootstrapTable('load', scope.facilities);
                    }
		});

		element.on('check.bs.table', (e, row) => {
                    scope.$apply(() => {
			scope.previousFacility = {code: row.code, name: row.name};
                    });
		});

		element.on('uncheck.bs.table', (e, row) => {
                    scope.$apply(() => {
			scope.previousFacility = null;
                    });
		});
            }

            return {
		link: link
            };
	}]);

    fracregDirectives.directive(
	'fracregPreviousProceduresTable',
	[() => {

            function link(scope, element, attrs) {
		element.data('striped', 'true');
		element.data('classes', 'table-condensed table');
		element.data('click-to-select', 'true');
		element.data('single-select', 'true');

		element.bootstrapTable({
                    data: [],
                    columns: [{
			field: 'state',
			checkbox: 'true'
                    }, {
			field: 'date',
			title: 'Operert'
                    }, {
			field: 'procedure',
			title: 'Skade/prosedyr'
                    }, {
			field: 'side',
			title: 'Side'
                    }]
		});

		scope.$watch('previousProcedures', () => {
                    element.bootstrapTable('load', scope.previousProcedures);
		});

		element.on('check.bs.table', (e, row) => {
                    scope.$apply(() => {
			scope.previousProcedure = row;
                    });
		});
            }

            return {
		link: link
            };
	}]);

    fracregDirectives.directive(
	'fracregSkeletonImage',
	[() => {
            function link(scope, element, attrs) {
		var that = this;

		$(element).find('#ao-skeleton-image')[0].addEventListener(
                    'load',
                    (event) => {
			var handle = Snap('#ao-skeleton-image');
			var clickLayer: Snap.Paper = <Snap.Paper>handle.select('g#click-layer');
			var rects = [];
			handle.selectAll('rect').forEach(
                            (r) => { rects.push(r); }
			);
			rects = _.filter(
                            rects,
                            (r) => {
				return r.node.id.search('ao-label-') == 0;
                            });

			_.each(rects,
                               (r) => {
				   var ao_code_prefix = r.node.id.substring(9, 11);

				   var bbox = r.getBBox();
				   var clickable = clickLayer.rect(
                                       bbox.x,
                                       bbox.y,
                                       bbox.width,
                                       bbox.height);

				   clickable.attr({
                                       fill: "#000000",
                                       opacity: 0,
                                       id: 'ao-clickable-' + ao_code_prefix
				   });

				   clickable.click(
                                       (evt) => {
                                           scope.aoPrefixClicked(
                                               evt.currentTarget.id.substring(13, 15));
                                       });
                               });
                    });
            }

            return {
		link: link
            };
	}]);

}
