/// <reference path="../lib/angularjs/angular.d.ts"/>
/// <reference path="../lib/underscore/underscore.d.ts"/>

'use strict';

/* Controllers */

module FracReg.Controller {

    var fracregControllers: angular.IModule = angular.module(
	'fracregControllers',
	['fracregServices',
         'ui.bootstrap']);

    interface Facility {
	code: string;
	name: string;
    }

    export interface Procedure {
	id: string;
	side: string;
	procedure: string;
	date: Date;
	hour: string; // TODO: Proper numeric type?
	delay: string; // TODO: Some sort of enum? This is a constrained value.
	ao_code: string;
	dislocation: string;
	open_fracture_classification: string;
	pathology: string;
	pathology_description: string;
    }

    // Information about an AO code
    export interface AOInfo {
	// Textual description of the code
	text: string;

	// The AO code itself
	id: string;
    };

    interface Pathology {
	text: string;
	id: string;
    };

    // Controller members that have a list of options for the value,
    // and a single selected value.
    interface SingleSelection<T> {
	selected: T;  // The selected value.
	options: T[]; // The list of options
    }

    // Controller members that have a list of options for the value,
    // and multiple selected values.
    interface MultiSelection<T> {
	selected: T[];  // The selected values.
	options: T[]; // The list of options
    }

    interface MultiSelectLocalization {
	selectAll: string;
	selectNone: string;
	reset: string;
	search: string;
	nothingSelected: string;
    }
    
    // Implementation of SingleSelection that starts with an empty
    // list of options and an undefined selected value.
    class DefaultSingleSelection<T> implements SingleSelection<T> {
	selected: T = null;
	options: T[] = [];
    }

    // Implementation of MultiSelection that starts with an empty
    // list of options and an empty list of selected values.
    class DefaultMultiSelection<T> implements MultiSelection<T> {
	selected: T[] = [];
	options: T[] = [];
    }
    
    export interface FracRegScope extends angular.IScope {	
	// Previous procedure
	previousFacility: Facility;
	previousProcedure: Procedure;
	previousExternalFacility: boolean;
	noneComplications: {id: string, text: string, selected: boolean}[];
	simpleComplications: {id: string, text: string, selected: boolean}[];
	seriousComplications: {id: string, text: string, selected: boolean}[];

        // Procedure info
        mainMethod: SingleSelection<string>;
        secondaryMethods: MultiSelection<string>;
        additionalProcedures: {id: string, text: string, selected: boolean}[];
        repositioning: SingleSelection<string>;
        arthroscopicallyAssisted: boolean;
        cast: SingleSelection<string>;
        woundTreatment: SingleSelection<string>;
        transplant: SingleSelection<string>;
        message: string;
	
	// Injury details
	aoInfo: AOInfo;
	hour: string;
	date: Date;

	treatmentDelay: SingleSelection<string>;
	dislocation: SingleSelection<string>;
	openFractureClassification: SingleSelection<string>;
	pathology: MultiSelection<Pathology>;
	pathologyDescription: string;

	// service stuff
	// TODO: Find a better way to model this.
	facilities: Facility[];
	previousProcedures: Procedure[];

	multiSelectLocalLang: MultiSelectLocalization;
	
	getSelected: (group: {selected: boolean}[]) => {selected: boolean}[];
	updateComplications: () => void;
        aoPrefixClicked: (prefix: string) => void;
    }

    // Assigned default values of a FracRegScope, and otherwise initialize it.
    function initializeScope(scope: FracRegScope): FracRegScope {
	scope.previousFacility = null;
	scope.previousProcedure = null;
	scope.previousExternalFacility = false;
	scope.noneComplications = [];
	scope.simpleComplications = [];
	scope.seriousComplications = [];

        // Procedure info
        scope.mainMethod = new DefaultSingleSelection<string>();
        scope.secondaryMethods = new DefaultMultiSelection<string>();
        scope.additionalProcedures = [];
        scope.repositioning = new DefaultSingleSelection<string>();
        scope.arthroscopicallyAssisted = false;
        scope.cast = new DefaultSingleSelection<string>();
	scope.woundTreatment = new DefaultSingleSelection<string>();
	scope.transplant = new DefaultSingleSelection<string>();
        scope.message = '';
	
	// Injury details
	scope.aoInfo = null;
	scope.hour = null;
	scope.date = null;

	scope.treatmentDelay = new DefaultSingleSelection<string>();
	scope.dislocation = new DefaultSingleSelection<string>();
	scope.openFractureClassification = new DefaultSingleSelection<string>();
	scope.pathology = new DefaultMultiSelection<Pathology>();
	scope.pathologyDescription = null;

	scope.facilities = [];
	scope.previousProcedures = [];

	scope.multiSelectLocalLang = {
	    selectAll       : "Velg alle",
	    selectNone      : "Velg ingen",
	    reset           : "Angre alle",
	    search          : "Søk...",
	    nothingSelected : "Velg..."
	}

	return scope;
    }
    
    // Returns an array of promises resolving to complications
    function getComplications(level, locations, resource) {
	return _.map(
            locations,
            (location)  => {
		return resource.query({location: location, level: level}).$promise;
            });
    }

    // The main controller for FracReg.
    //
    // Coordinates the primary data in the application, and maintains
    // data retrieved from various providers.
    fracregControllers.controller(
	'FracRegCtrl',
	['$scope', '$http', '$q', '$modal',
	 'PreviousProcedures',
	 'Facilities',
	 'SecondaryProcedureReasons',
	 'AOCodes',
	 'Dislocations',
	 ($scope: FracRegScope, $http, $q, $modal,
	  PreviousProcedures,
	  Facilities,
	  SecondaryProcedureReasons,
	  AOCodes,
	  Dislocations) => {
	      $scope = initializeScope($scope);

	      // Updates the "complications" based on previousFacility
	      $scope.updateComplications = () => {
		  _.each(['none', 'simple', 'serious'],
			 (level) => {
			     var attr = level + 'Complications';
			     if ($scope.previousFacility) {
				 var requests = getComplications(level, [$scope.previousFacility.code], SecondaryProcedureReasons);
				 $q.all(requests).then((results) => {
				     $scope[attr] = _.map<{id: string, text: string},
				                          {id: string, text: string, selected: boolean}>(
					 _.flatten(results),
					 (r) => {
					     return {id: r.id, text: r.text, selected: false};
					 });
				 });
			     }
			     else {
				 $scope[attr] = [];
			     }
			     
			 });
	      };

              $scope.aoPrefixClicked = (prefix: string) => {
                  var modalInstance = $modal.open({
    		      templateUrl: 'aoSelectionDialog.html',
    		      controller: 'AOSelectionInstanceCtrl',
                      resolve: {
                          prefix: () => { return prefix; }
                      }
    		  });

                  modalInstance.result.then(function (aoInfo) {
                      $scope.aoInfo = aoInfo;
                  }, function () {
                      // $log.info('Modal dismissed at: ' + new Date());
                  });
              };
              
              var facilities = Facilities.query({}, () => {
		  $scope.facilities = facilities;
              });

	      // When a facility changes, we update the available
	      // procedures and complications.
              $scope.$watch('previousFacility', () => {
		  if ($scope.previousFacility) {
                      var procedures = PreviousProcedures
			  .query({facility: $scope.previousFacility.code}, () => {
                              $scope.previousProcedures = procedures;
			  });
		  }
		  else {
                      $scope.previousProcedures = [];
		  }

		  $scope.updateComplications();
              });

	      // When a procedure is selected, we fill in the
	      // pertinent scope elements to reflect the procedure.
              $scope.$watch('previousProcedure', () => {
		  if ($scope.previousProcedure) {
                      var info = AOCodes.get(
			  {code: $scope.previousProcedure.ao_code},
			  () => {
                              $scope.aoInfo = info;
			  });

                      $scope.date = new Date($scope.previousProcedure.date.getTime());
                      $scope.hour = $scope.previousProcedure.hour;
                      $scope.treatmentDelay.selected = _.findWhere(
			  $scope.treatmentDelay.options,
			  {id: $scope.previousProcedure.delay});

                      $scope.dislocation.selected = _.findWhere(
			  $scope.dislocation.options,
			  {id: $scope.previousProcedure.dislocation});

                      $scope.openFractureClassification.selected = _.findWhere(
			  $scope.openFractureClassification.options,
			  {id: $scope.previousProcedure.open_fracture_classification});

                      $scope.pathology.selected = _.filter(
			  $scope.pathology.options,
			  (p) => {
                              return _.contains($scope.previousProcedure.pathology, p.id);
			  });

                      $scope.pathologyDescription = $scope.previousProcedure.pathology_description;
		  }
              });

              // Treatment delay
              $http.get('/treatment_delays').then(
		  (response) => {
                      $scope.treatmentDelay.options = response.data;
                      $scope.treatmentDelay.selected = $scope.treatmentDelay.options[0];
		  },
		  (response) => {
                      // TODO: Something useful.
		  }
              );

              // Dislocations
              $scope.$watch('aoInfo', () => {
		  if ($scope.aoInfo) {
                      var dislocations = Dislocations
			  .query({ao_code: $scope.aoInfo.id}, () => {
                              $scope.dislocation.options = dislocations;
			  });
		  }
              });

              // Open fracture classification
              $http.get('/open_fracture_classifications').then(
		  (response) => {
                      $scope.openFractureClassification.options = response.data;
		  },
		  (response) => {
                      // TODO: Something useful.
		  }
              );

              // Pathologies
              $http.get('/pathologies').then(
		  (response) => {
                      $scope.pathology.options = response.data;
		  },
		  (response) => {
                      // TODO: Something useful.
		  }
              );

	      // Main/secondary methods
              $http.get('/main_methods').then(
		  (response) => {
                      $scope.mainMethod.options = response.data;
		      $scope.secondaryMethods.options = response.data;
		  },
		  (response) => {
                      // TODO: Something useful.
		  }
              );

	      // Additional procedures
	      $http.get('/additional_procedures').then(
		  (response) => {
		      $scope.additionalProcedures = _.map<{id: string, text: string},
		                                          {id: string, text: string, selected: boolean}>(
			  response.data,
			  (r) => {
			      return {id: r.id, text: r.text, selected: false};
			  });
		  },
		  (response) => {
		      // TODO: Something useful.
		  }
	      );

	      // Open fracture classification
              $http.get('/repositioning').then(
		  (response) => {
                      $scope.repositioning.options = response.data;
		      $scope.repositioning.selected = $scope.repositioning.options[0];
		  },
		  (response) => {
                      // TODO: Something useful.
		  }
              );

	      // Cast
              $http.get('/cast').then(
		  (response) => {
                      $scope.cast.options = response.data;
		      $scope.cast.selected = $scope.cast.options[0];
		  },
		  (response) => {
                      // TODO: Something useful.
		  }
              );

	      // Cast
              $http.get('/cast').then(
		  (response) => {
                      $scope.cast.options = response.data;
		      $scope.cast.selected = $scope.cast.options[0];
		  },
		  (response) => {
                      // TODO: Something useful.
		  }
              );

	      // Wound treatment
              $http.get('/wound_treatment').then(
		  (response) => {
                      $scope.woundTreatment.options = response.data;
		      $scope.woundTreatment.selected = $scope.woundTreatment.options[0];
		  },
		  (response) => {
                      // TODO: Something useful.
		  }
              );

	      // Transplant
              $http.get('/transplants').then(
		  (response) => {
                      $scope.transplant.options = response.data;
		      $scope.transplant.selected = $scope.transplant.options[0];
		  },
		  (response) => {
                      // TODO: Something useful.
		  }
              );

	      $scope.getSelected = function (group) {
		  return _.filter(
		      group,
		      (g) => { return g.selected }
		  );
	      }
	  }]);

}
