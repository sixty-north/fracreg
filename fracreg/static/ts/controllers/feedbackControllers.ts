/// <reference path="../lib/angularjs/angular.d.ts"/>
/// <reference path="../lib/angular-ui-bootstrap/angular-ui-bootstrap.d.ts"/>
/// <reference path="../lib/underscore/underscore.d.ts"/>

'use strict';

module FracReg.Feedback {

    var fracregFeedbackControllers: angular.IModule = angular.module(
	'fracregFeedbackControllers',
	['ui.bootstrap']);
    
    // The small scope needed for opening the feedback dialog.
    export interface FracRegFeedbackScope extends angular.IScope {
    	open: () => void;
    }

    // Controller for opening the feedback dialog and storing any
    // results.
    fracregFeedbackControllers.controller(
    	'FracRegFeedbackCtrl',
    	['$scope', '$http', '$modal',
    	 ($scope: FracRegFeedbackScope,
    	  $http: angular.IHttpService,
    	  $modal: angular.ui.bootstrap.IModalService) => {
    	     $scope.open = () => {
		 
    		 var modalInstance = $modal.open({
    		     templateUrl: 'fracregFeedback.html',
    		     controller: 'FracRegFeedbackInstanceCtrl'
    		 });
	 
    		 modalInstance.result.then(
    		     function (values) {
			 if (values.message.length > 0)
			 {
    			     $http.post<any>(
    	      			 "/feedback",
    	      			 {name: values.name,
    	      			  message: values.message});
			 }
    		     },
    		     function () {
    		     });
    	     }
    	  }]);

    // Data storage and show/hide semantics for an instance of the
    // feedback dialog.
    export interface FracRegFeedbackInstanceScope extends angular.IScope {
	// The name of the person submitting feedback
    	name: string;

	// Their message.
    	message: string;

	// Available dialog actions.
    	ok: () => void;
    	cancel: () => void;
    }
    
    fracregFeedbackControllers.controller(
    	'FracRegFeedbackInstanceCtrl',
    	['$scope', '$modalInstance',
    	 ($scope: FracRegFeedbackInstanceScope,
    	  $modalInstance: angular.ui.bootstrap.IModalServiceInstance) => {
    	      $scope.name = '';
    	      $scope.message = '';
	      
    	      $scope.ok = () => {
    	      	  $modalInstance.close({
    		      name: $scope.name,
    		      message: $scope.message
    		  });
    	      };
	      
    	      $scope.cancel = () => {
    	      	  $modalInstance.dismiss('cancel');
    	      };
    	 }]);    
    
}
