/// <reference path="../lib/angularjs/angular.d.ts"/>
/// <reference path="../lib/angular-ui-bootstrap/angular-ui-bootstrap.d.ts"/>
/// <reference path="../lib/underscore/underscore.d.ts"/>
/// <reference path="fracregControllers.ts"/>

'use strict';

module FracReg.AOSelection {
    var aoSelectionControllers: angular.IModule = angular.module(
        'aoSelectionControllers',
        ['ui.bootstrap.modal',
         'fracregServices']);
    
    export interface AOSelectionInstanceScope extends angular.IScope {
        prefixedAoCodes: FracReg.Controller.AOInfo[];

        // Available dialog actions.
    	ok: () => void;
    	cancel: () => void;
    }
    
    aoSelectionControllers.controller(
    	'AOSelectionInstanceCtrl',
    	($scope: any,
    	 $modalInstance: angular.ui.bootstrap.IModalServiceInstance,
         AOCodes,
         prefix: string) => {
             var all_codes: [FracReg.Controller.AOInfo] = AOCodes.query({}, () => {
                 // Find all ao-codes that match the current prefix
                 $scope.prefixedAoCodes = _.filter(
                     all_codes,
                     (c) => {
                         return (c.id.substring(0, 2) == prefix);
                     });
             });
	     
    	     $scope.select = (aoInfo: FracReg.Controller.AOInfo) => {
    	      	 $modalInstance.close(aoInfo);
    	     };
	     
    	     $scope.cancel = () => {
    	      	 $modalInstance.dismiss('cancel');
    	     };
    	 });    
}
