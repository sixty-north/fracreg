<div>
    <script type="text/ng-template" id="aoSelectionDialog.html">
        <div class="modal-header">
	    <h4 class="modal-title">Velg AO Kode</h4>
	</div>
	<div class="modal-body row">
	    <div ng-repeat="ao_info in prefixedAoCodes">
		<div class="col-md-3">
		    <img src="/ao_image/{{ao_info.id}}" height="50px" ng-click="select(ao_info)"/><br>
		    <label>{{ao_info.id}}</label>
		</div>
	    </div>
	</div>
	<div class="modal-footer">
	    <button class="btn btn-warning" ng-click="cancel()">Cancel</button>
	</div>
    </script>
</div>

<div class="container-fluid">
    <div class="row">
	<div class="col-md-4">
	    <div class="panel panel-default">
		<div class="panel-heading">Velg fraktur</div>
		<div class="panel-body">
		    <fracreg-skeleton-image>
			<object type="image/svg+xml" id="ao-skeleton-image" data="static/images/skeleton.svg" height="700px">Your browser does not support SVG</object>
		    </fracreg-skeleton-image>
		</div>
	    </div>
	</div>

	<div class="col-md-8">
	    <div id="ao-info-display"></div>
	    <div class="form">
		<div class="panel panel-default">
		    <div class="panel-heading">AO Kode</div>
		    <div class="panel-body">
			<div class="row">
			    <div class="col-md-8">
				<h3>
				    <div ng-if="aoInfo">
					<label>Valgt:</label> {{aoInfo.text}} {{aoInfo.id}}
				    </div>
				    <div ng-if="!aoInfo">
					<label>Ikke valgt (obligatorisk)</label>
				    </div>
				</h3>
			    </div>
			    <div class="col-md-4">
				<img src="/ao_image/{{aoInfo.id}}" height="100">
			    </div>
			</div>
		    </div>
		</div>
		<div class="panel panel-default">
		    <div class="panel-heading">Tid</div>
		    <div class="panel-body">
			<div class="form-group">
			    <label for="injury-time-input" class="control-label">Skadetidspunkt</label>
			    <input type="number" name="quantity" min="0" max="23" ng-model="hour">
			</div>
			<div class="form-group">
			    <label for="injury-date-input" class="control-label">Traumadato</label>
			    <input type="date" class="form-control" ng-model="date"/>
			</div>
			<div class="form-group">
			    <label for="treatment-delay-select" class="control-label">Forsinkelse</label>

			    <ui-select ng-model="treatmentDelay.selected">
				<ui-select-match allow-clear="false">{{$select.selected.text}}</ui-select-match>
				<ui-select-choices repeat="delay in treatmentDelay.options">
				    <div ng-bind-html="delay.text"></div>
				</ui-select-choices>
			    </ui-select>
			</div>
		    </div>
		</div>
		<div class="panel panel-default">
		    <div class="panel-heading">Luksasjon/subluksasjon</div>
		    <div class="panel-body">
			<ui-select ng-model="dislocation.selected" ng-disabled="aoInfo === null" theme="bootstrap">
			    <ui-select-match allow-clear="true" placeholder="Velg...">{{$select.selected.text}}</ui-select-match>
			    <ui-select-choices repeat="dislocation in dislocation.options">
				<div ng-bind-html="dislocation.text"></div>
			    </ui-select-choices>
			</ui-select>
		    </div>
		</div>

		<div class="panel panel-default">
		    <div class="panel-heading">Åpen fraktur</div>
		    <div class="panel-body">
			<ui-select ng-model="openFractureClassification.selected" ng-disabled="aoInfo === null" theme="bootstrap">
			    <ui-select-match allow-clear="true" placeholder="Velg...">{{$select.selected.text}}</ui-select-match>
			    <ui-select-choices repeat="classification in openFractureClassification.options">
				<div ng-bind-html="classification.text"></div>
			    </ui-select-choices>
			</ui-select>
		    </div>
		</div>

		<div class="panel panel-default">
		    <div class="panel-heading">Patologisk/periimplantat/ost. imperfekta</div>
		    <div class="panel-body">
			<div isteven-multi-select
                             input-model="pathology.options"
                             output-model="pathology.selected"
                             button-label="text"
                             item-label="text"
                             tick-property="selected"
                             style="width: 100%;"
                             translation="multiSelectLocalLang",
                             is-disabled="aoInfo === null">
			</div>
			<textarea ng-model="pathologyDescription" ng-disabled="aoInfo === null" class="form-control">
			</textarea>
		    </div>
		</div>
	    </div>
	</div>
    </div>
</div>
