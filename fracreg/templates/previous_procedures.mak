<div class="container-fluid">
    <div class="row">

        <!-- facility/procedure selection on one half of screen. -->
        <div class="col-md-6">
            <div class="row">
	        <div class="panel panel-default">
		    <div class="panel-heading">Anlegg</div>
		    <div class="panel-body">
		        <table fracreg-facilities-table>
			    <thead>
			        <tr>
				    <th data-field="state" data-checkbox="true"></th>
				    <th data-field="facility" data-align="right">Facility</th>
			        </tr>
			    </thead>
		        </table>

		    </div>
                </div>
            </div>
            <div class="row">
                <div class="panel panel-default">
		    <div class="panel-heading">Prosedyr</div>
		    <div class="panel-body">
		        <table fracreg-previous-procedures-table>
			    <thead>
			        <tr>
				    <th data-field="state" data-checkbox="true"></th>
				    <th data-field="facility" data-align="right">Facility</th>
			        </tr>
			    </thead>
		        </table>
		    </div>
	        </div>
            </div>
        </div>

        <!-- complications on another half -->
        <div class="col-md-6">
	    <div ng-if="previousFacility">
	        <div class="row">

                    <div class="col-md-12">
		        <div class="panel panel-default">
		            <div class="panel-heading">Ikke komplikasjoner</div>
		            <div class="panel-body">
			        <form>
			            <div ng-repeat="comp in noneComplications">
				        <input type="checkbox"
				               value="{{comp.text}}"
				               ng-model="comp.selected"> {{comp.text}}
			            </div>
			        </form>
		            </div>
		        </div>
                    </div>
                </div>

	        <div class="row">
                    <div class="col-md-12">
		        <div class="panel panel-default">
		            <div class="panel-heading">Lett komplikasjoner</div>
		            <div class="panel-body">
			        <form>
			            <div ng-repeat="comp in simpleComplications">
				        <input type="checkbox"
				               value="{{comp.text}}"
				               ng-model="comp.selected"> {{comp.text}}
			            </div>
			        </form>
		            </div>
		        </div>
                    </div>
	        </div>

	        <div class="row">
                    <div class="col-md-12">
		        <div class="panel panel-default">
		            <div class="panel-heading">Alvorlig komplikasjoner</div>
		            <div class="panel-body">
			        <form>
			            <div ng-repeat="comp in seriousComplications">
				        <input type="checkbox"
				               value="{{comp.text}}"
				               ng-model="comp.selected"> {{comp.text}}
			            </div>
			        </form>
		            </div>
		        </div>
                    </div>
	        </div>

	    </div>
        </div>
    </div>
</div>
