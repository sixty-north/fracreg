<!-- This is a simple system for letting users provide feedback on the system.
This adds a button called "Feedback" which, when pressed, brings up a
dialog box that let's them enter feedback. -->

<hr>

<!-- the dialog box -->
<div class="row" ng-controller="FracRegFeedbackCtrl">
    <div class="col-md-offset-10 col-md-1">
	<script type="text/ng-template" id="fracregFeedback.html">
	    <div class="modal-header">
		<h4 class="modal-title">Tell us what you think!</h4>
	    </div>
	    <div class="modal-body">
		<div class="form-group">
		    <label for="feedback-name-text">Name</label>
		    <input type="text" class="form-control" ng-model="name" placeholder="Your name (optional)">
		</div>
		<div class="form-group">
		    <label for="feedback-body-textarea">Message</label>
		    <textarea class="form-control" ng-model="message" placeholder="Your message"></textarea>
		</div>
	    </div>
	    <div class="modal-footer">
		<button class="btn btn-warning" ng-click="cancel()">Cancel</button>
		<button class="btn btn-primary" ng-click="ok()" ng-disabled="message.length == 0">Send feedback</button>
	    </div>
	</script>

	<button class="btn btn-default" ng-click="open()">Feedback</button>
    </div>
</div>
