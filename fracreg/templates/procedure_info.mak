<!-- template for procedure info -->
<div class="row">
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">Metoder</div>
            <div class="panel-body">
                <div class="form-group">
                    <label>Hovedmetode</label>
		    <ui-select ng-model="mainMethod.selected" theme="bootstrap">
			<ui-select-match allow-clear="true" placeholder="Ikke valgt...">{{$select.selected.text}}</ui-select-match>
			<ui-select-choices repeat="method in mainMethod.options">
			    <div ng-bind-html="method.text"></div>
			</ui-select-choices>
		    </ui-select>
                </div>

                <div class="form-group">
                    <label>Tillegsmetoder</label>
		    <div     
			    isteven-multi-select
			    input-model="secondaryMethods.options"
			    output-model="secondaryMethods.selected"
			    button-label="text"
			    item-label="text"
			    tick-property="selected"
			    style="width: 100%;"
			    translation="multiSelectLocalLang",
			    >
		    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">Tillegsprosedyrer</div>
            <div class="panel-body">
                <div ng-repeat="proc in additionalProcedures">
		    <input type="checkbox"
			   value="{{proc.text}}"
			   ng-model="proc.selected"> {{proc.text}}
		</div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">Detaljer</div>
            <div class="panel-body">
                <div class="form-group">
                    <label>Reponering</label>
		    <ui-select ng-model="repositioning.selected" theme="bootstrap">
			<ui-select-match allow-clear="false">{{$select.selected.text}}</ui-select-match>
			<ui-select-choices repeat="r in repositioning.options">
			    <div ng-bind-html="r.text"></div>
			</ui-select-choices>
		    </ui-select>
                </div>
                <div class="form-group">
		    <input type="checkbox" ng-model="arthroscopicallyAssisted"> <label>Artroskopisk assistert</label>
                </div>
                <div class="form-group">
                    <label class="control-label">Gips</label>
		    <ui-select ng-model="cast.selected" theme="bootstrap">
			<ui-select-match allow-clear="false">{{$select.selected.text}}</ui-select-match>
			<ui-select-choices repeat="c in cast.options">
			    <div ng-bind-html="c.text"></div>
			</ui-select-choices>
		    </ui-select>
                </div>
                <div class="form-group">
                    <label class="control-label">Sårbehandling</label>
		    <ui-select ng-model="woundTreatment.selected" theme="bootstrap">
			<ui-select-match allow-clear="false">{{$select.selected.text}}</ui-select-match>
			<ui-select-choices repeat="w in woundTreatment.options">
			    <div ng-bind-html="w.text"></div>
			</ui-select-choices>
		    </ui-select>
                </div>
                <div class="form-group">
                    <label class="control-label">Transplantasjon</label>
		    <ui-select ng-model="transplant.selected" theme="bootstrap">
			<ui-select-match allow-clear="false">{{$select.selected.text}}</ui-select-match>
			<ui-select-choices repeat="t in transplant.options">
			    <div ng-bind-html="t.text"></div>
			</ui-select-choices>
		    </ui-select>
                    <div id="transplant-display"></div>
                </div>
                <div class="form-group">
                    <label class="control-label">Beskjed</label>
		    <textarea class="form-control" ng-model="message"></textarea>
                </div>
            </div>
        </div>
    </div>
</div>
