<h1>Oppsummering</h1>

<div class="row">
    <div class="col-md-3">
	<div class="panel panel-default">
	    <div class="panel-heading">Tidligere behandling</div>
	    <div class="panel-body">

		<form class="form">
		    <div class="form-group">
			<label class="control-label">Prosedyr</label>
			<p class="form-control-static">
			    <div ng-if="previousProcedure">
				{{previousProcedure.date}} - {{previousProcedure.procedure}} - {{previousProcedure.side}}
			    </div>
			</p>
		    </div>
		    <div class="form-group">
			<label class="control-label">Anlegget</label>
			<p class="form-control-static">
			    <div ng-if="previousFacility">
				{{previousFacility.name}}
			    </div>
			</p>
		    </div>
		    <div class="form-group">
			<label class="control-label">Sekondærprosedyr årsak</label>
			<div>			    
			    <!-- <div ng-if="getSelected(noneComplications).length > 0"> -->
			    <div ng-if="(noneComplications | filter: {selected: true}).length > 0 ">
				<em>Ikke komplikasjon</em>
				<ul ng-repeat="c in noneComplications | filter: {selected: true}">
				    <li>{{c.text}}</li>
				</ul>
			    </div>

			    <div ng-if="(simpleComplications | filter: {selected: true}).length > 0 ">
				<em>Lett komplikasjon</em>
				<ul ng-repeat="c in simpleComplications | filter: {selected: true}">
				    <li>{{c.text}}</li>
				</ul>
			    </div>

			    <div ng-if="(seriousComplications | filter: {selected: true}).length > 0 ">
				<em>Alvorlig komplikasjon</em>
				<ul ng-repeat="c in seriousComplications | filter: {selected: true}">
				    <li>{{c.text}}</li>
				</ul>
			    </div>
			</div>
		    </div>

		</form>

	    </div>
	</div>
    </div>
    <div class="col-md-3">
	<div class="panel panel-default">
	    <div class="panel-heading">Skade detaljer</div>
	    <div class="panel-body">

		<form class="form">
		    <div class="form-group">
			<label class="control-label">AO Kode</label>
			<div ng-if="aoInfo === null">
			    <p class="form-control-static text-danger"><strong>OBLIGATORISK</strong></p>
			</div>
			<div ng-if="aoInfo !== null">
			    <p class="form-control-static">{{aoInfo.id}}</p>
			</div>
		    </div>
		    <div class="form-group">
			<label class="control-label">Skade tid og dato</label>
			<p class="form-control-static">kl. {{hour}} på {{date.toLocaleDateString()}}</p>
		    </div>
		    <div class="form-group">
			<label class="control-label">Forsinkelse</label>
			<p class="form-control-static">
			    <div ng-if="treatmentDelay.selected">
				{{treatmentDelay.selected.text}}
			    </div>
			</p>
		    </div>
		    <div class="form-group">
			<label class="control-label">Luksasjon/subluksasjon</label>
			<p class="form-control-static">
			    <div ng-if="dislocation.selected">
				{{dislocation.selected.text}}
			    </div>
			</p>
		    </div>
		    <div class="form-group">
			<label class="control-label">Åpen fraktur klassifisering</label>
			<p class="form-control-static">
			    <div ng-if="openFractureClassification.selected">
				{{openFractureClassification.selected.text}}
			    </div>
			</p>
		    </div>
		    <div class="form-group">
			<label class="control-label">Patologi</label>
			<p class="form-control-static">
			    <div ng-repeat="p in pathology.selected">
				{{p.text}}
			    </div>
			</p>
		    </div>
		    <div class="form-group">
			<label class="control-label">Patologi detaljer</label>
			<div ng-if="pathologyDescription !== null">
			    {{pathologyDescription}}
			</div>
		    </div>
		</form>

	    </div>
	</div>

    </div>
    <div class="col-md-3">
	<div class="panel panel-default">
	    <div class="panel-heading">Operasjon detaljer</div>
	    <div class="panel-body">

		<form class="form">
		    <div class="form-group">
			<label class="control-label">Hovedmetode</label>
			<!-- TODO: Capture this notion of required fields in a directive? -->
			<div ng-if="!mainMethod.selected">
			    <p class="form-control-static text-danger"><strong>OBLIGATORISK</strong></p>
			</div>
			<div ng-if="mainMethod.selected"">
			    <p class="form-control-static">{{mainMethod.selected.text}}</p>
			</div>
		    </div>
		    <div class="form-group">
			<label class="control-label">Tillegsmetoder</label>
			<ul ng-repeat="m in secondaryMethods.selected track by $index">
			    <li>{{m.text}}</li>
			</ul>
		    </div>
		    <div class="form-group">
			<label class="control-label">Tillegsprosedyrer</label>
			<ul ng-repeat="p in additionalProcedures | filter:{selected: true}">
			    <li>{{p.text}}</li>
			</ul>
		    </div>
		    <div class="form-group">
			<label class="control-label">Reponering</label>
			<div ng-if="repositioning.selected">
			    <p class="form-control-static">{{repositioning.selected.text}}</p>
			</div>
		    </div>
		    <div class="form-group">
			<label class="control-label">Artroskopisk assistert</label>
			<p class="form-control-static">
			    <div ng-if="arthroscopicallyAssisted">Ja</div>
			    <div ng-if="!arthroscopicallyAssisted">Nei</div>
			</p>
		    </div>
		    <div class="form-group">
			<label class="control-label">Gips</label>
			<div ng-if="cast.selected">
			    <p class="form-control-static">{{cast.selected.text}}</p>
			</div>
		    </div>
		    <div class="form-group">
			<label class="control-label">Sårbehandling</label>
			<div ng-if="woundTreatment.selected">
			    <p class="form-control-static">{{woundTreatment.selected.text}}</p>
			</div>
		    </div>
		    <div class="form-group">
			<label class="control-label">Transplantasjon</label>
			<div ng-if="transplant.selected">
			    <p class="form-control-static">{{transplant.selected.text}}</p>
			</div>
		    </div>
		    <div class="form-group">
			<label class="control-label">Beskjed</label>
			<p class="form-control-static">{{message}}</p>
		    </div>
		</form>

	    </div>
	</div>
    </div>
    <div class="col-md-3">
	<div class="panel panel-default">
	    <div class="panel-heading">Koder</div>
	    <div class="panel-body">
		<div class="form-group">
		    <label class="control-label">ICD-10</label>
		    <p class="form-control-static">S72.40</p>
		</div>
		<div class="form-group">
		    <label class="control-label">NCSP</label>
		    <p class="form-control-static">NFJ15</p>
		    <p class="form-control-static">NFJ65</p>
		</div>
	    </div>
	</div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
	<button id="submit-button" type="submit" class="btn btn-primary btn-lg">Submit</button>
    </div>
</div>
