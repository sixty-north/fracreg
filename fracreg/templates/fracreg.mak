<!doctype html>
<html lang="en" ng-app="fracregApp">
    <head>
	<meta charset="utf-8">
	<title>FracReg</title>
	<link rel="stylesheet" href="static/bower_components/bootstrap/dist/css/bootstrap.css">
	<link rel="stylesheet" href="static/bower_components/bootstrap-table/dist/bootstrap-table.min.css">
	<link rel="stylesheet" href="static/bower_components/angular-ui-select/dist/select.min.css">
	<link rel="stylesheet" href="static/bower_components/angular-multi-select/isteven-multi-select.css">
	<link rel="stylesheet" href="static/css/app/fracreg.css">

	<script src="static/bower_components/jquery/dist/jquery.min.js"></script>
	<script src="static/bower_components/angular/angular.js"></script>
	<script src="static/bower_components/angular-route/angular-route.js"></script>
	<script src="static/bower_components/angular-resource/angular-resource.js"></script>
	<script src="static/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
	<script src="static/bower_components/bootstrap-table/dist/bootstrap-table.min.js"></script>
	<script src="static/bower_components/underscore/underscore-min.js"></script>
	<script src="static/bower_components/snap.svg/dist/snap.svg-min.js"></script>
	<script src="static/bower_components/angular-sanitize/angular-sanitize.min.js"></script>
	<script src="static/bower_components/angular-ui-select/dist/select.min.js"></script>
	<script src="static/bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js"></script>
	<script src="static/bower_components/angular-multi-select/isteven-multi-select.js"></script>
	<script src="static/js/built/app.js"></script>
        <script src="static/js/built/controllers/aoSelectionDialogController.js"></script>
	<script src="static/js/built/controllers/fracregControllers.js"></script>
	<script src="static/js/built/controllers/feedbackControllers.js"></script>
	<script src="static/js/built/directives.js"></script>
	<script src="static/js/built/services.js"></script>
    </head>
    <body ng-app>
	<div class="container-fluid" ng-controller="FracRegCtrl">
	    <div class="row">
		<h1>Frakturregister</h1>
	    </div>
	    <div class="row">
		<div role="tabpanel">
		    <!-- Nav tabs -->
		    <ul class="nav nav-tabs" role="tablist">
			<li role="presentation" class="active"><a href="#previous-procedures" aria-controls="previous-procedures" role="tab" data-toggle="tab">Tidligere prosedyrer</a></li>
			<li role="presentation"><a href="#injury-info" aria-controls="injury-info" role="tab" data-toggle="tab">Skade detaljer</a></li>
			<li role="presentation"><a href="#procedure-info" aria-controls="procedure-info" role="tab" data-toggle="tab">Prosedyr detaljer</a></li>
			<li role="presentation"><a href="#summary" aria-controls="summary" role="tab" data-toggle="tab">Oppsumering</a></li>
		    </ul>

		    <!-- Tab panes -->
		    <div class="tab-content">
			<div role="tabpanel" class="tab-pane active" id="previous-procedures">
			    <%include file="previous_procedures.mak"/>
			</div>

			<div role="tabpanel" class="tab-pane" id="injury-info">
			    <%include file="injury_info.mak"/>
			</div>

			<div role="tabpanel" class="tab-pane" id="procedure-info">
			    <%include file="procedure_info.mak"/>
			</div>

			<div role="tabpanel" class="tab-pane" id="summary">
			    <%include file="summary.mak"/>
			</div>
		    </div>
		</div>
	    </div>
	</div>

	<%include file="feedback_dialog.mak"/>
	
    </body>
</html>
