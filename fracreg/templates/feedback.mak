<!doctype html>
<html lang="en" ng-app="fracregApp">
    <head>
	<meta charset="utf-8">
	<title>FracReg feedback</title>
    </head>
    <body>
	<a href="/">home</a>

	<table>
	    <tr>
		<th>Name</th>
		<th>Message</th>
		<th>Timestamp</th>
	    </tr>
	    % for fb in feedback:
		<tr>
		    <td>${fb[0]}</td>
		    <td>${fb[1]}</td>
		    <td>${fb[2]}</td>
		</tr>
	    % endfor
	</table>
    </body>
</html>
