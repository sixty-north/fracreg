import functools


def memoize(obj):
    """A decorator that memoizes return values.

    This caches the return values of the decorated function, returning
    the cached values when identical arguments are supplied to the
    function.
    """
    cache = obj.cache = {}

    @functools.wraps(obj)
    def memoizer(*args, **kwargs):
        key = str(args) + str(kwargs)
        if key not in cache:
            cache[key] = obj(*args, **kwargs)
        return cache[key]

    return memoizer
