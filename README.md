fracreg
=======

fracreg is a web-based system for registering fracture information after operations.

fracreg uses a number of technologies including:

 * Python3.4+
 * Pyramid
 * Angular
 * Bootstrap
 * TypeScript
 * Snap.svg

AO Code images
==============

Right now it's not clear which (if any) images we can bundle with
fracreg for display of AO codes. As a result, we've cleared out the
directory `fracreg/static/ao-images`. If you have access to an image
set you'd like to use, you can put them in that directory following
using the following naming scheme:

```
<code in caps without spaces>.jpg
```

For example, the image file for AO code "32A1" should be
`fracreg/static/ao-images/32A1.jpg`. See also
`fracreg/static/ao-images/README.txt`.

Quickstart
==========

First, read the section above about AO code images.

Generally the simplest way to run fracreg is to create a virtual
environment, install all of the dependencies there, and run the server
out of the virtual environment. Once you've cloned the repository,
change to the directory created during the clone. This should contain
a file called `setup.py`.

The steps to get the system running are:

1. Create and/or activate a Python3 virtual environment
2. Install the Python dependencies
3. Install the node.js dependencies (primarily TypeScript)
4. Run the pyramid server

From this directory, create a virtual environment, activate that
environment, install all of the dependencies, and then launch the
development server using `pserve`. That all looks something like this:

```
$ cd fracreg
$ pyvenv -p python3 venv
$ source venv/bin/activate
(venv) $ venv/bin/python setup.py install
(venv) $ npm install
(venv) $ venv/bin/pserve development.ini --reload
```

At this point you should be able to access fracreg in your browser at
`http://localhost:6543`.

There are, of course, other ways to set this up, but these directions
should at least get you started.

Testing
=======

The `fracreg` automated test suite is based on
[jasmine](https://github.com/jasmine/jasmine) and uses the
[karma](http://karma-runner.github.io) test runner. To run the tests,
go to the root of the project and run:
```
karma start karma.conf.js
```

This will launch a browser and execute the tests.

Other methods may be created in the future, e.g. for simply dumping
test results to a file.
